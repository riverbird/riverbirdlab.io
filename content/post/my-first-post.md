---
title: "久违的自由的感觉"
date: "2018-10-01"
description: "生活很无趣，找到他的乐趣。"
lead: "生活很无趣，找到他的乐趣。"
disable_comments: false # Optional, disable Disqus comments if true
authorbox: true # Optional, enable authorbox for specific post
toc: true # Optional, enable Table of Contents for specific post
mathjax: true # Optional, enable MathJax for specific post
categories:
  - "生活"
tags:
  - "思考"
  - "感想"
menu: side # Optional, add page to a menu. Options: main, side, footer
---
## 工作的自由
我在好久一段时间以来，工作受到我的所谓的“客户”的安排。因为我们是一条业务线，但不是一个业务组，所以很多时候明显他在给你穿小鞋。我想过离职，因为实在受不了他对我的挤兑。不过有一天，我的新的老板告诉我，他不是你客户，你作为项目经理，要为项目负责，做的好与不好，是项目说了算，而不是他说了算，于是，我感觉到了自由。我开始站在项目的角度去思考问题，而不用去顾及他的感受，甚至于敢怼他，我觉得，因为没有了枷锁，内心自由，所以工作也变得不那么痛苦起来。

## 感情的自由
我被我现在的妻子严格控制或者说是管理起来已经有5年之久，这5年来，我做任何事她基本上都反对，我去外面玩得她同意，我去见朋友她也跟着去，我的微信她要经常查看，说实话我非常累。直到有一天，我觉得我不必要这么累，大不了离婚，何苦呢？所以，TMD，随他去吧，我想玩想见什么人，就凭我喜欢，不用顾及她的感受。

## 生活的自由
因为一直以来经济上面其实是捉襟见肘的，所以，一直在担心，如果不工作吃什么？以及，如果出来重新找工作，能不能找到一份更好的工作？因为一直这个问题，所以8年来，自己基本上不敢动，变得跟鹌鹑一样，只有去扮演一个好同事，好父亲，好儿子，好老公，好朋友等角色，但是却逐渐迷失了自我。现在我在想，我今年35岁，不管将来生活会怎样，我要勇敢一点，做真正的自己！
