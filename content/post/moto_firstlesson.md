---
title: "关于机车的第一课"
date: "2018-04-02"
description: "关于机车的第一课"
lead: "关于机车的第一课"
disable_comments: false # Optional, disable Disqus comments if true
authorbox: true # Optional, enable authorbox for specific post
toc: true # Optional, enable Table of Contents for specific post
mathjax: true # Optional, enable MathJax for specific post
categories:
  - "摩行"
tags:
  - "机车"
  - "摩旅"
menu: side # Optional, add page to a menu. Options: main, side, footer
---
如果要想在某个领域快速成长，没有什么比跟着这人领域的大牛混来的更快一些。

2018年3月31日，在摩托帮上面组织了5个人的队伍，一起从杭州出发，浩浩荡荡杀到余姚四明山。虽然，最后一位宁波的兄弟本来打算周五晚上骑过来跟我们汇合，后来因为临时有事没有来，但是，其它人全部陆陆续续走在一起了。

一路游荡，本来我是组织者，最后发现自己原来是最菜的那个。车菜，人菜，汗一个。共它的几位新朋友的座驾是春风400.，川崎250TR，本田大踏板250，本田190，唯一自己的只有150，有点小，跟起来有点费劲。在5档油门到底的时候，极速是100码。

来来去去共计320公里，跟着各路大神混，收到的经验如下：

1. 如果要玩车，最好从250的街车起步；
2. ABS非常重要，关键时刻能救命；
3. 许多人在市区里跑，虽然市区禁摩，最重要的是不要撞在警察怀里；
4. 玩车主要是速度与激情，和开车绝对是两回事，因此超速、夹塞，在所难免；
5. 护具很重要，蓝牙头盔，骑行服，都很重要；
6. 虽然改装的前后档比较方便，但是最安全的还是国际标准的上抬档；
7. 天下摩友是一家，重要的是玩本身；
8. 中度以上上瘾者，一般会经常换车，这个在所难免。