---
title: "我的机车，我的新女友"
date: "2018-03-15"
description: "我的机车，我的新女友"
lead: "我的机车，我的新女友"
disable_comments: false # Optional, disable Disqus comments if true
authorbox: true # Optional, enable authorbox for specific post
toc: true # Optional, enable Table of Contents for specific post
mathjax: true # Optional, enable MathJax for specific post
categories:
  - "摩行"
tags:
  - "机车"
  - "摩旅"
menu: side # Optional, add page to a menu. Options: main, side, footer
---
换了很多辆车，只记得第一辆车子的时候异常兴奋，到后来逐渐麻木。
而现在，我的第一辆摩托车-suzuki 悦酷gz150，将即归入我怀报。很兴奋，整整一周，恨不得立刻在它身边。
每个人对摩托车的感情是不一样的，而我，天生对各自驾驶超感兴趣，而摩托车，我惦记了好多好多年。

跑车对自己来讲太过年轻，从来只是会试试，但不愿意长久骑，也许跟年龄有关。作为35岁的大叔，我更喜欢哈雷的经典，那种腔调是我所追求的。但是囊中羞涩，我只能退而求其次。在经历了以前汽车的质量不好的气愤之后，对于摩托车我更大的要求是耐操，而在所有的选择中，铃木的悦酷gz150，是目前唯一也是最打动我的心的。铃木的质量、外观、配置，俘获了我的心，我看了那么多车子，唯一对这辆车子念念不忘。现在，终于有了回响，很期待，很兴奋。

我知道这辆车加上装备，太过简陋，甚至还算不上机车的入门。但是至少现在已经满足，想想最快周六就将驾驶着它去浪，去征服山川河海，乡间小道，让自己不由自主的期待时间过的更快一些。

我有很多兴趣，很多爱好。我希望能赚更多的钱，为这些兴趣做更多的投资。生活，本该丰富多彩，不是吗？