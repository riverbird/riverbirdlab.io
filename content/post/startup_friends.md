---
title: "我身边的创业伙伴"
date: "2018-11-07"
description: "创业，有进无退！"
lead: "创业，有进无退！"
disable_comments: false # Optional, disable Disqus comments if true
authorbox: true # Optional, enable authorbox for specific post
toc: true # Optional, enable Table of Contents for specific post
mathjax: true # Optional, enable MathJax for specific post
categories:
  - "创业"
tags:
  - "创业"
  - "调研"
menu: side # Optional, add page to a menu. Options: main, side, footer
---
2018年注定不是平凡的一年。

我在工作中挣扎了好久之后，毅然选择了辞职，我知道，是时间去真正面对自己了。
走访和了解了一下部分前同事的创业情况，做一个简单的小结。

## 真数科技
产品：布林证券：致力于为客户提供“一站式”的海外证券资产投资服务，通过提供遍及全球的金融资产的交易平台及相应的行情资讯、投资资讯、社区、策略超市等服务，帮助投资人实现资产增值。
发展：2016.12.31上线，2017.4.1A轮。
人员：目前已发展到40-50人的团队。
同事：戴流珩，林巍，李崇，吴建南

## 西猫网络
产品：宠物社交，主打宠物社交、交配。
发展：2018.6月起步，目前还在草创阶段。
人员：6人左右，含运营、产品、财务，另外几个开发。
同事：尤家华

## 广度金融
产品：木瓜金融，一个金融客户端；
同事：李笑飞，应晓胜，柳兵

## 辉江科技
产品；信用卡服务工具
发展：2018.10月起步，目前还在草创阶段。
人员：目前2人，纯技术。
同事：裘启坤，葛伟

## 蛋糕店
产品：自营蛋糕店
发展：2018.4月开了冰淇淋店，现在开蛋糕店；
人员：2人，老板加一位雇员。
同事：鲍芳容