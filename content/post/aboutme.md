---
title: "关于"
date: "2018-11-01"
description: "关于我"
lead: "关于我"
disable_comments: false # Optional, disable Disqus comments if true
authorbox: true # Optional, enable authorbox for specific post
toc: true # Optional, enable Table of Contents for specific post
mathjax: true # Optional, enable MathJax for specific post
menu: main # Optional, add page to a menu. Options: main, side, footer
---
我本楚狂人，凤歌笑孔丘。 手持绿玉杖，朝别黄鹤楼。

五岳寻仙不辞远，一生好入名山游。

庐山秀出南斗傍，屏风九叠云锦张， 影落明湖青黛光。

金阙前开二峰长，银河倒挂三石梁。

香炉瀑布遥相望，回崖沓嶂凌苍苍。

翠影红霞映朝日，鸟飞不到吴天长。

登高壮观天地间，大江茫茫去不还。

黄云万里动风色，白波九道流雪山。