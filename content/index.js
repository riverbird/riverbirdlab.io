var map = new AMap.Map('container', {
    resizeEnable: true,
    center: [120.119759, 30.272042],
    zoom: 10
});

// 工具栏
AMap.plugin('AMap.ToolBar', function () {
    var toolbar = new AMap.ToolBar();
    map.addControl(toolbar)
})

//marker标记-景点
addMarker();
function addMarker() {
    map.clearMap();
    var infoWindow = new AMap.InfoWindow({ offset: new AMap.Pixel(0, -30) });
    for (var i = 0, marker; i < pos_info.length; i++) {
        var marker = new AMap.Marker({
            position: pos_info[i].pos,
            map: map
        });
        marker.content = pos_info[i].title;
        marker.on('click', markerClick);
        marker.emit('click', { target: marker });
    }
    function markerClick(e) {
        infoWindow.setContent(e.target.content);
        infoWindow.open(map, e.target.getPosition());
    }
    map.setFitView();
}

// 圆点标记-城市
for (var i = 0; i < capitals.length; i += 1) {
    var center = capitals[i].center;
    var new_radius = capitals[i].radius;
    var circleMarker = new AMap.CircleMarker({
        center: center,
        /*radius: 10 + Math.random() * 10,//3D视图下，CircleMarker半径不要超过64px*/
        radius: 10 + new_radius,
        strokeColor: 'white',
        strokeWeight: 2,
        strokeOpacity: 0.5,
        fillColor: 'rgba(0,0,255,1)',
        fillOpacity: 0.5,
        zIndex: 10,
        bubble: true,
        cursor: 'pointer',
        clickable: true
    })
    circleMarker.setMap(map)
}

